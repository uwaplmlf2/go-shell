package shell

import (
	"context"
	"strings"
	"testing"
)

var INPUT = `echo "hello world"
echo 1 "2 3" 4

echo 1 "2" 3 4
`

var OUTPUT = `"hello world"
1 "2 3" 4

1 2 3 4
`

var SHORT_OUTPUT = `"hello world"
1 "2 3" 4
`

type port struct {
	*strings.Builder
	*strings.Reader
}

func newPort() *port {
	p := port{}
	p.Builder = &strings.Builder{}
	p.Reader = strings.NewReader(INPUT)
	return &p
}

func TestRun(t *testing.T) {
	p := newPort()
	s := New(p, "")

	err := s.Run(context.Background())
	if err != nil {
		t.Error(err)
	}

	output := p.String()
	if output != OUTPUT {
		t.Errorf("Expected %q, got %q", OUTPUT, output)
	}
}

func TestExitOnEmpty(t *testing.T) {
	p := newPort()
	opts := []Option{ExitOnEmpty(true)}
	s := New(p, "", opts...)

	err := s.Run(context.Background())
	if err != nil {
		t.Error(err)
	}

	output := p.String()
	if output != SHORT_OUTPUT {
		t.Errorf("Expected %q, got %q", SHORT_OUTPUT, output)
	}
}
