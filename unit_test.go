package shell

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"testing"
	"time"
)

func busy(ctx context.Context, args ...string) (string, error) {
	time.Sleep(time.Second * 2)
	return "Done", nil
}

func checkPort(ctx context.Context, args ...string) (string, error) {
	_, ok := PortFromContext(ctx)
	if !ok {
		return "", fmt.Errorf("Port not found")
	}
	return "found", nil
}

func TestRunOne(t *testing.T) {
	var buf bytes.Buffer
	s := New(&buf, "> ")

	input := `testing 1 "2 3" 4`
	output, err := s.RunOne(context.Background(), "echo "+input)
	if err != nil {
		t.Fatal(err)
	}

	if output != input {
		t.Errorf("Wrong output; expected %q, got %q", input, output)
	}

	output, err = s.RunOne(context.Background(), "foo")
	if err != nil || output != "" {
		t.Errorf("Unknown command not ignored")
	}
}

func TestDefault(t *testing.T) {
	opts := []Option{SetDefault(func(ctx context.Context, args ...string) (string, error) {
		return "hello " + args[0], nil
	})}
	var buf bytes.Buffer
	s := New(&buf, "> ", opts...)

	output, err := s.RunOne(context.Background(), "world")
	if err != nil {
		t.Fatal(err)
	}
	expect := "hello world"
	if output != expect {
		t.Errorf("Wrong output; expected %q, got %q", expect, output)
	}
}

func TestStrict(t *testing.T) {
	opts := []Option{Strict(true)}
	var buf bytes.Buffer
	s := New(&buf, "> ", opts...)

	input := `testing 1 "2 3" 4`
	output, err := s.RunOne(context.Background(), "echo "+input)
	if err != nil {
		t.Fatal(err)
	}

	if output != input {
		t.Errorf("Wrong output; expected %q, got %q", input, output)
	}

	output, err = s.RunOne(context.Background(), "eChO")
	if err == nil {
		t.Errorf("Unknown command not caught")
	}
}

func TestNoCase(t *testing.T) {
	opts := []Option{Strict(true), CaseInsensitive(true)}
	var buf bytes.Buffer
	s := New(&buf, "> ", opts...)

	input := `testing 1 "2 3" 4`
	output, err := s.RunOne(context.Background(), "eChO "+input)
	if err != nil {
		t.Fatal(err)
	}

	if output != input {
		t.Errorf("Wrong output; expected %q, got %q", input, output)
	}
}

func TestNoBuiltin(t *testing.T) {
	opts := []Option{Strict(true), NoBuiltin(true), Strict(true)}
	var buf bytes.Buffer
	s := New(&buf, "> ", opts...)

	input := `testing 1 "2 3" 4`
	_, err := s.RunOne(context.Background(), "echo "+input)
	if !errors.Is(err, ErrUnknownCmd) {
		t.Errorf("Unexpected error value: %v", err)
	}
}

func TestPortPassing(t *testing.T) {
	var buf bytes.Buffer
	s := New(&buf, "> ")

	s.AddCommand("check", checkPort)
	_, err := s.RunOne(context.Background(), "check")
	if err != nil {
		t.Fatal(err)
	}
}

func TestAsync(t *testing.T) {
	var buf bytes.Buffer
	s := New(&buf, "> ")

	s.AddCommand("busy", busy)
	err := s.async(context.Background(), "busy")
	if err != nil {
		t.Fatal(err)
	}

	err = s.async(context.Background(), "busy")
	if err == nil {
		t.Fatalf("Async error not caught")
	}

	resp, err := s.await(context.Background(), "busy")
	if err != nil {
		t.Fatal(err)
	}

	if resp != "Done" {
		t.Errorf("Invalid response; expected %q, got %q\n", "DONE", resp)
	}

	if s.futures["busy"] != nil {
		t.Errorf("Async cleanup not done\n")
	}
}

func TestAsyncTimeout(t *testing.T) {
	var buf bytes.Buffer
	s := New(&buf, "> ")

	s.AddCommand("busy", busy)
	err := s.async(context.Background(), "busy")
	if err != nil {
		t.Fatal(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_, err = s.await(ctx, "busy")
	if err == nil {
		t.Fatalf("Timeout not caught")
	}
}
