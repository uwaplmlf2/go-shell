// Package shell provides a framework for a simple command interpreter.
package shell

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"

	"github.com/google/shlex"
	"github.com/pkg/errors"
)

type Result struct {
	Out string
	Err error
}

// Command is a command executed by the interpreter. It should
// check for a cancellation signal from the supplied Context.
type Command func(ctx context.Context, args ...string) (string, error)

var ErrUnknownCmd = errors.New("Invalid command")

type UnknownCommand struct {
	cmd string
}

func (e *UnknownCommand) Error() string {
	return e.cmd + ": invalid command"
}

// Options accepted by the New function.
type Option struct {
	f func(*shellOptions)
}

type shellOptions struct {
	emptyExit, allowExternal, strict bool
	noBuiltin                        bool
	quietStart                       bool
	noCase                           bool
	rawIO                            bool
	ignoreEOF                        bool
	env                              map[string]string
	defaultCmd                       Command
}

// ExitOnEmpty forces the shell to exit if a blank line is input
func ExitOnEmpty(state bool) Option {
	return Option{func(opts *shellOptions) {
		opts.emptyExit = state
	}}
}

// AllowExternal allows the execution of external commands
func AllowExternal(state bool) Option {
	return Option{func(opts *shellOptions) {
		opts.allowExternal = state
	}}
}

// Strict forces the shell to exit on an unknown command
func Strict(state bool) Option {
	return Option{func(opts *shellOptions) {
		opts.strict = state
	}}
}

// IgnoreEOF ignores EOF on the input
func IgnoreEOF(state bool) Option {
	return Option{func(opts *shellOptions) {
		opts.ignoreEOF = state
	}}
}

// ShellEnv sets environment variables for the shell
func ShellEnv(env map[string]string) Option {
	return Option{func(opts *shellOptions) {
		opts.env = env
	}}
}

// QuietStart does not issue a prompt at startup
func QuietStart(state bool) Option {
	return Option{func(opts *shellOptions) {
		opts.quietStart = state
	}}
}

// CaseInsensitive ignores the case of input commands
func CaseInsensitive(state bool) Option {
	return Option{func(opts *shellOptions) {
		opts.noCase = state
	}}
}

func RawIO(state bool) Option {
	return Option{func(opts *shellOptions) {
		opts.rawIO = state
	}}
}

// Disable builtin shell commands (exit, await, async, echo)
func NoBuiltin(state bool) Option {
	return Option{func(opts *shellOptions) {
		opts.noBuiltin = state
	}}
}

// SetDefault specifies a default command to run when there is no exact
// command match. Note that this effectively disables the AddExternal
// option.
func SetDefault(cmd Command) Option {
	return Option{func(opts *shellOptions) {
		opts.defaultCmd = cmd
	}}
}

// Shell is a command interpreter
type Shell struct {
	port          io.ReadWriter
	emptyExit     bool
	allowExternal bool
	beStrict      bool
	quietStart    bool
	noCase        bool
	rawIO         bool
	noBuiltin     bool
	ignoreEOF     bool
	prompt        []byte
	builtin       map[string]Command
	cmds          map[string]Command
	defCmd        Command
	futures       map[string]chan Result
	cancel        context.CancelFunc
}

type key int

const shellPortKey key = 0

// Extract the Shell i/o port from a Context, if present
func PortFromContext(ctx context.Context) (io.ReadWriter, bool) {
	port, ok := ctx.Value(shellPortKey).(io.ReadWriter)
	return port, ok
}

// Create a function to run an external command
func makeExternal(prog string) Command {
	return Command(func(ctx context.Context, args ...string) (string, error) {
		cmd := exec.CommandContext(ctx, prog, args...)
		result, err := cmd.CombinedOutput()
		return string(result), err
	})
}

func joinWithQuotes(args []string) string {
	var b strings.Builder
	for i, a := range args {
		if i > 0 {
			b.WriteString(" ")
		}
		if idx := strings.IndexAny(a, " \t"); idx >= 0 {
			fmt.Fprintf(&b, "%q", a)
		} else {
			b.WriteString(a)
		}
	}
	return b.String()
}

// Create a new Shell connected to an io.ReadWriter. Prompt is the command prompt
// string and env is used to modify environment variables.
func New(port io.ReadWriter, prompt string, options ...Option) *Shell {
	opts := shellOptions{}
	for _, opt := range options {
		opt.f(&opts)
	}

	s := Shell{
		port:    port,
		prompt:  []byte(prompt),
		builtin: make(map[string]Command),
		cmds:    make(map[string]Command),
		futures: make(map[string]chan Result),
	}

	if opts.env != nil {
		for k, v := range opts.env {
			os.Setenv(k, v)
		}
	}
	s.emptyExit = opts.emptyExit
	s.allowExternal = opts.allowExternal
	s.beStrict = opts.strict
	s.quietStart = opts.quietStart
	s.noCase = opts.noCase
	s.rawIO = opts.rawIO
	s.noBuiltin = opts.noBuiltin
	s.defCmd = opts.defaultCmd
	s.ignoreEOF = opts.ignoreEOF

	s.builtin["async"] = func(ctx context.Context, args ...string) (string, error) {
		return "", s.async(ctx, args...)
	}
	s.builtin["await"] = func(ctx context.Context, args ...string) (string, error) {
		return s.await(ctx, args[0])
	}
	s.builtin["echo"] = func(ctx context.Context, args ...string) (string, error) {
		return joinWithQuotes(args), nil
	}
	s.builtin["exit"] = func(ctx context.Context, args ...string) (string, error) {
		s.exitRun()
		return "", nil
	}

	s.builtin["quit"] = s.builtin["exit"]

	return &s
}

// Run a command asynchronously
func (s *Shell) async(ctx context.Context, args ...string) error {
	f := s.getCommand(args[0], false)

	if ch := s.futures[args[0]]; ch == nil {
		ch = make(chan Result, 1)
		s.futures[args[0]] = ch
		go func() {
			defer close(ch)
			result, err := f(ctx, args[1:]...)
			ch <- Result{Out: result, Err: err}
		}()
		return nil
	}

	return errors.New("Permission denied")
}

// Wait for an asynchronous command to finish.
func (s *Shell) await(ctx context.Context, name string) (string, error) {
	ch := s.futures[name]
	if ch == nil {
		return "", errors.New("Permission denied")
	}

	defer delete(s.futures, name)

	select {
	case <-ctx.Done():
		return "", ctx.Err()
	case result := <-ch:
		return result.Out, result.Err
	}
}

// Exit from Shell.Run by calling the cancel function
func (s *Shell) exitRun() {
	if s.cancel != nil {
		s.cancel()
	}
}

// AddCommand adds a new command function to the Shell.
func (s *Shell) AddCommand(name string, cmd Command) {
	if s.noCase {
		name = strings.ToLower(name)
	}
	s.cmds[name] = cmd
}

func (s *Shell) getCommand(name string, allowBuiltin bool) Command {
	if s.noCase {
		name = strings.ToLower(name)
	}

	if allowBuiltin {
		if cmd, ok := s.builtin[name]; ok {
			return cmd
		}
	}

	if cmd, ok := s.cmds[name]; ok {
		return cmd
	}

	if s.defCmd != nil {
		// The default command gets the command name as its first argument.
		return func(ctx context.Context, args ...string) (string, error) {
			cmdargs := []string{name}
			cmdargs = append(cmdargs, args...)
			return s.defCmd(ctx, cmdargs...)
		}
	}

	if s.allowExternal {
		return makeExternal(name)
	}

	if !s.beStrict {
		return func(ctx context.Context, args ...string) (string, error) {
			return "", nil
		}
	}

	return func(ctx context.Context, args ...string) (string, error) {
		return "", fmt.Errorf("%s: %w", name, ErrUnknownCmd)
	}
}

func readLine(rdr io.Reader) (string, error) {
	c := make([]byte, 1)
	buf := make([]byte, 0)
	n, err := rdr.Read(c)
	for n > 0 {
		buf = append(buf, c[0])
		if bytes.ContainsAny(buf, "\r\n") {
			break
		}
		n, err = rdr.Read(c)
		if err != nil {
			break
		}
	}
	return string(buf), err
}

func readUntil(rdr io.Reader, match []byte) (string, error) {
	c := make([]byte, 1)
	buf := make([]byte, 0)
	n, err := rdr.Read(c)
	for n > 0 {
		buf = append(buf, c[0])
		if bytes.HasSuffix(buf, match) {
			break
		}
		n, err = rdr.Read(c)
		if err != nil {
			break
		}
	}
	return string(buf), err
}

// Run runs a shell session until a command parsing error occurs, an exit
// command is run, or the Context is cancelled.
func (s *Shell) Run(pctx context.Context) error {
	var ctx context.Context

	ctx, s.cancel = context.WithCancel(pctx)
	ctx = context.WithValue(ctx, shellPortKey, s.port)

	defer func() {
		s.cancel()
		s.cancel = nil
	}()

	var (
		output string
		tokens []string
	)

	if !s.quietStart {
		s.port.Write(s.prompt)
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		text, err := readLine(s.port)
		if err == io.EOF {
			if !s.ignoreEOF {
				return nil
			}
			continue
		}

		if err != nil {
			return err
		}

		if len(text) == 0 {
			continue
		}

		tokens, err = shlex.Split(text)
		if err != nil {
			return err
		}

		if len(tokens) > 0 {
			if s.rawIO {
				s.port.Write([]byte("\r\n"))
			}
			cmd := s.getCommand(tokens[0], !s.noBuiltin)
			if len(tokens) > 1 {
				output, err = cmd(ctx, tokens[1:]...)
			} else {
				output, err = cmd(ctx)
			}

			if err != nil {
				fmt.Fprintf(s.port, "ERROR: %s\n", err.Error())
			} else {
				fmt.Fprintln(s.port, output)
			}
		} else {
			if s.emptyExit {
				break
			}
			if s.rawIO {
				s.port.Write([]byte("\r\n"))
			} else {
				s.port.Write([]byte("\n"))
			}
		}

		s.port.Write(s.prompt)
	}

	return nil
}

// RunOne executes a single command, returning the commands output and
// any error that occurs.
func (s *Shell) RunOne(ctx0 context.Context, text string) (string, error) {
	tokens, err := shlex.Split(text)
	if err != nil {
		return "", err
	}
	if len(tokens) == 0 {
		return "", nil
	}
	cmd := s.getCommand(tokens[0], !s.noBuiltin)

	ctx := context.WithValue(ctx0, shellPortKey, s.port)
	if len(tokens) > 1 {
		return cmd(ctx, tokens[1:]...)
	}
	return cmd(ctx)
}
