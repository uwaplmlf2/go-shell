module bitbucket.org/uwaplmlf2/go-shell

require (
	github.com/google/shlex v0.0.0-20181106134648-c34317bd91bf
	github.com/pkg/errors v0.8.0
)

go 1.13
